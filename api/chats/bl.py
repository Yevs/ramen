import json

import redis as redis_lib

from django.utils.functional import SimpleLazyObject

from .models import Message
from .constants import MESSAGES_TOPIC


connection_pool = redis_lib.BlockingConnectionPool.from_url(
    'redis://redis:6379',
    max_connections=10
)
redis = SimpleLazyObject(lambda: redis_lib.Redis(connection_pool=connection_pool))


def publish_message(message):
    redis.publish(MESSAGES_TOPIC, json.dumps(message.serialize()))


def send_message(user_id, meeting_event_id, content):
    message = Message.objects.create(
        sender_id=user_id,
        meeting_event_id=meeting_event_id,
        content=content
    )
    publish_message(message)
    return message
