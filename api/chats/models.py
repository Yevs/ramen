from django.db import models
from django.utils import timezone

from passport.models import User
from meetings.models import MeetingEvent

from .constants import MAX_MESSAGE_LEN


class Message(models.Model):
    class Meta:
        db_table = 'messages'

    content = models.CharField(max_length=MAX_MESSAGE_LEN, null=False, blank=False)
    sender = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    meeting_event = models.ForeignKey(
        MeetingEvent, null=False, on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=True)

    def serialize(self):
        return {
            'content': self.content,
            'sender_id': self.sender and self.sender.id,
            'meeting_event_id': self.meeting_event.id,
            'created_at': str(self.created_at)
        }

    @property
    def is_active(self):
        now = timezone.now()
        return self.date >= now.date() and self.end < now.hour

