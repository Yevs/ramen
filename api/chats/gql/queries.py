import graphene

from gql_app.exceptions import GQLError
from meetings.models import MeetingEvent
from meetings.bl import active_meetings

from ..models import Message
from .nodes import MessageNode


class ChatQuery(graphene.ObjectType):
    messages = graphene.List(
        graphene.NonNull(MessageNode),
        required=True
    )

    def resolve_messages(self, info):
        meeting = active_meetings(info.context.user.id).first()
        if not meeting:
            raise GQLError(f'No active event now')

        return Message.objects.filter(meeting_event=meeting).order_by('created_at')
