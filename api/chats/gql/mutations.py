import graphene

from gql_app.exceptions import GQLError
from meetings.models import MeetingEvent
from meetings.bl import active_meetings

from .nodes import MessageNode
from ..bl import send_message
from ..constants import MAX_MESSAGE_LEN


class SendMessage(graphene.Mutation):
    class Arguments:
        content = graphene.String(required=True)

    message = graphene.Field(graphene.NonNull(MessageNode))

    def mutate(self, info, *, content):
        meeting = active_meetings(info.context.user.id).first()
        if not meeting:
            raise GQLError(f'No active meeting')
        stripped = content.strip()
        if not stripped:
            raise GQLError(f'Can\'t send empty message')
        if len(stripped) > MAX_MESSAGE_LEN:
            raise GQLError(f'Message length exceeds max length of {MAX_MESSAGE_LEN} characters')

        return SendMessage(message=send_message(
            info.context.user.id,
            meeting.id,
            stripped
        ))


class ChatMutations(graphene.ObjectType):
    send_message = SendMessage.Field(required=True)
