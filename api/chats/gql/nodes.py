from graphene_django import DjangoObjectType

from ..models import Message


class MessageNode(DjangoObjectType):
    class Meta:
        model = Message
        only_fields = [
            'id',
            'content',
            'sender',
            'meeting_event',
            'created_at'
        ]
