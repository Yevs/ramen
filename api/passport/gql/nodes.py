import jwt
import graphene

from graphene_django import DjangoObjectType

from django.conf import settings

from gql_app.exceptions import GQLForbidden

from passport.models import User
from meetings.bl import awaiting_queries, active_meetings


class UserNode(DjangoObjectType):
    class Meta:
        model = User
        only_fields = ['id', 'first_name', 'last_name', 'avatar']

    def resolve_last_name(self, info):
        if info.context.user.id != self.id:
            raise GQLForbidden('Cannot query other\'s last name')
        return self.last_name


class ViewerStatus(graphene.Enum):
    WELCOME = 'WELCOME'
    SLOT = 'SLOT'  # form page
    LOOKING = 'LOOKING'  # loader
    CHAT = 'CHAT'  # chat


class ViewerNode(graphene.ObjectType):
    user = graphene.Field(UserNode)
    status = graphene.Field(ViewerStatus, required=True)
    ws_auth_token = graphene.Field(graphene.String, required=True)

    def resolve_user(self, info):
        if not info.context.user.is_authenticated:
            return None
        return info.context.user

    def resolve_status(self, info):
        user = info.context.user
        if not user.is_authenticated:
            return ViewerStatus.WELCOME
        if active_meetings(user.id).exists():
            return ViewerStatus.CHAT
        if awaiting_queries(user.id).exists():
            return ViewerStatus.LOOKING
        return ViewerStatus.SLOT

    def resolve_ws_auth_token(self, info):
        return jwt.encode(
            {'user_id': info.context.user.id},
            settings.WS_AUTH_SECRET,
            algorithm='HS256'
        ).decode('utf-8')
