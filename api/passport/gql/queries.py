import graphene

from passport.models import User

from .nodes import UserNode, ViewerNode


class PassportQuery(graphene.ObjectType):
    viewer = graphene.Field(ViewerNode, required=True)
    user = graphene.Field(UserNode, id=graphene.Int(required=True))

    def resolve_viewer(root, info):
        return ViewerNode()

    def resolve_user(root, info, *, id):
        try:
            return User.objects.get(pk=id)
        except User.DoesNotExist:
            return None
