from django.contrib import admin
from django.urls import path, include, re_path
from django.http import HttpResponse

from graphene_django.views import GraphQLView

from .views import index


urlpatterns = [
    path('admin/', admin.site.urls),
    path('graphql/', GraphQLView.as_view(graphiql=True)),
    re_path(r'^',  include('social_django.urls', namespace='social')),
    re_path(r'^', index, name='index')
]
