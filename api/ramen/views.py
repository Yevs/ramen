import requests

from django.conf import settings
from django.views.decorators.cache import cache_page
from django.http import HttpResponse


@cache_page(60)
def index(request):
    response = requests.get(settings.GS_BUCKET_URL + '/index.html')
    return HttpResponse(response)
