import os
from pathlib import Path

from dotenv import load_dotenv


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

env_path = Path(BASE_DIR) / 'config' / '.env'

load_dotenv(dotenv_path=env_path)


DEBUG = False
WS_AUTH_SECRET = os.environ['WS_AUTH_SECRET']
SECRET_KEY = os.environ['SECRET_KEY']
SOCIAL_AUTH_FACEBOOK_KEY = os.environ['SOCIAL_AUTH_FACEBOOK_KEY']
SOCIAL_AUTH_FACEBOOK_SECRET = os.environ['SOCIAL_AUTH_FACEBOOK_SECRET']
SOCIAL_AUTH_LOGIN_REDIRECT_URL = 'https://gezer.co/looking'
SOCIAL_AUTH_SANITIZE_REDIRECTS = False
SOCIAL_AUTH_REDIRECT_IS_HTTPS = True


CORS_ORIGIN_WHITELIST = (
    'gezer.co',
)

