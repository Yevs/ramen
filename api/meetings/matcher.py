from django.db import transaction
from django.db.models import Q, F

from meetings.models import MeetingRequest, MeetingEvent
from meetings.constants import (
    MIN_EVENT_PARTICIPANTS,
    MAX_EVENT_PARTICIPANTS
)


def make_default_value():
    starts = list(range(0, 26, 2))
    ends = [start + 2 for start in starts]
    return [
        (start, end, [])
        for start, end in zip(starts, ends)
    ]


def create_meeting_event(meeting_requests):
    start = max(request.start for request in meeting_requests)
    end = min(request.end for request in meeting_requests)
    event = MeetingEvent.objects.create(
        date=meeting_requests[0].date,
        start=start,
        end=end,
    )
    event.users.add(*(request.user_id for request in meeting_requests))
    MeetingRequest.objects \
        .filter(id__in=[r.id for r in meeting_requests])\
        .update(is_matched=True)
    return event


@transaction.atomic
def create_instance_match(meeting_request):
    query = (
        Q(date=meeting_request.date) &
        Q(start__lte=meeting_request.start) &
        Q(end__gt=meeting_request.start) &
        Q(is_matched=False) &
        ~Q(user=meeting_request.user)
    )
    ordering = F('end') - F('start')
    other_requests = list(
        MeetingRequest.objects.filter(query).order_by(ordering)
        [:MAX_EVENT_PARTICIPANTS]
    )
    if len(other_requests) < MIN_EVENT_PARTICIPANTS - 1:
        return None
    return create_meeting_event([meeting_request] + other_requests)


def submit_request(user, date, start, end):
    meeting_request = MeetingRequest.objects.create(
        user=user,
        date=date,
        start=start,
        end=end
    )
    return meeting_request
