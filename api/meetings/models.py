from django.core.validators import MaxValueValidator
from django.db import models

from passport.models import User


max_hours_validator = MaxValueValidator(24)


class MeetingEvent(models.Model):
    class Meta:
        db_table = 'meeting_event'

    users = models.ManyToManyField(User, related_name='meeting_events')
    date = models.DateField(blank=False, null=False)
    start = models.PositiveSmallIntegerField(
        validators=[max_hours_validator],
        null=False
    )
    end = models.PositiveSmallIntegerField(
        validators=[max_hours_validator],
        null=False
    )
    created_at = models.DateTimeField(auto_now_add=True)


class MeetingRequest(models.Model):
    class Meta:
        db_table = 'meeting_request'
        # unique_together = ('user', 'date', 'start', 'end')

    user = models.ForeignKey(
        User,
        null=True,
        on_delete=models.SET_NULL,
        related_name='meeting_request'
    )
    date = models.DateField(blank=False, null=False)
    start = models.PositiveSmallIntegerField(
        validators=[max_hours_validator],
        null=False,
        blank=False
    )
    end = models.PositiveSmallIntegerField(
        validators=[max_hours_validator],
        null=False,
        blank=False
    )
    is_matched = models.BooleanField(null=False, blank=False, default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def timeslots_key(self):
        return f'{self.date.year}-{self.date.month}-{self.date.day}'
