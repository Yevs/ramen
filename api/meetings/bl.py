import pytz

from django.db.models import Q
from django.utils import timezone
from django.utils.timezone import localtime

from .models import MeetingRequest, MeetingEvent


def get_now():
    tz = pytz.timezone('Europe/Kiev')
    return timezone.localtime(timezone.now(), timezone=tz)


def awaiting_queries(user_id):
    now = get_now()
    return MeetingRequest.objects.filter(
        Q(is_matched=False) &
        Q(user=user_id) &
        Q(
            Q(date__gt=now.date()) |
            Q(
                Q(date=now.date()) &
                Q(end__gt=now.hour)
            )
        )
    )


def get_is_active_query(user_id):
    now = get_now()
    return (
        Q(users=user_id) &
        Q(
            Q(date__gt=now.date()) |
            Q(
                Q(date=now.date()) &
                Q(end__gt=now.hour)
            )
        )
    )


def active_meetings(user_id):
    return MeetingEvent.objects.filter(get_is_active_query(user_id))
