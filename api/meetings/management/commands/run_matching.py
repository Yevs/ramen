from datetime import datetime

from django.core.management.base import BaseCommand

from meetings.models import MeetingRequest
from meetings.matcher import create_instance_match


class Command(BaseCommand):
    help = 'Run matching algorithm'

    def handle(self, *args, **kwargs):
        for meeting_request in MeetingRequest.objects.filter(is_matched=False):
            meeting_request.refresh_from_db()
            if meeting_request.is_matched:
                continue
            create_instance_match(meeting_request)
        print(f'Ran match @ {datetime.now()}')
