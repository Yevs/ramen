import random
import uuid

from faker import Faker

from django.core.management.base import BaseCommand
from django.utils import timezone

from passport.models import User
from meetings.models import MeetingRequest


class Command(BaseCommand):
    help = 'Create fake data'

    def add_arguments(self, parser):
        parser.add_argument(
            '--users',
            dest='user_count',
            type=int,
            default=10,
            help='number of random users to generate'
        )
        parser.add_argument(
            '--events',
            dest='event_count',
            type=int,
            default=10,
            help='number of random events to generate'
        )
        parser.add_argument(
            '--seed',
            dest='seed',
            type=int,
            help='random seed'
        )

    def handle(self, *args, user_count, event_count, **kwargs):
        faker = Faker()
        if 'seed' in kwargs:
            seed = kwargs['seed']
            faker.seed(seed)
            random.seed(seed)
        users = []
        for _ in range(user_count):
            while True:
                username = faker.user_name()
                if not User.objects.filter(username=username).exists():
                    break
            users.append(User.objects.create(
                password=str(uuid.uuid4()),
                username=username,
                first_name=faker.first_name(),
                last_name=faker.last_name()
            ))
        for _ in range(event_count):
            start = random.randint(0, 20)
            delta = random.randint(2, 4)
            end = start + delta
            MeetingRequest.objects.create(
                date=timezone.now().date(),
                start=start,
                end=end,
                user=random.choice(users)
            )
