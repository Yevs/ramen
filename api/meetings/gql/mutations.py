from dateutil import parser

import graphene

from gql_app.exceptions import GQLError
from gql_app.decorators import login_required

from .inputs import CreateMeetingRequestInput
from .nodes import MeetingRequestNode, MeetingEventNode
from ..matcher import submit_request, create_instance_match
from ..bl import awaiting_queries, active_meetings


class CreateMeetingRequest(graphene.Mutation):
    class Arguments:
        input = graphene.Argument(CreateMeetingRequestInput, required=True)

    meeting_request = graphene.Field(MeetingRequestNode, required=True)
    meeting_event = graphene.Field(MeetingEventNode)

    @login_required
    def mutate(self, info, *, input):
        if input.start >= input.end:
            raise GQLError('Start must be greater than end')
        if awaiting_queries(info.context.user.id).exists():
            raise GQLError('You already have awaiting queries')
        valid_slots = [
            (12, 18),
            (18, 22),
            (22, 24)
        ]
        if (input.start, input.end) not in valid_slots:
            raise GQLError('Invalid slot')
        datetime = parser.parse(input.date)
        date = datetime.date()
        meeting_request = submit_request(
            start=input.start,
            end=input.end,
            date=date,
            user=info.context.user
        )
        meeting_event = create_instance_match(meeting_request)
        return CreateMeetingRequest(
            meeting_request=meeting_request,
            meeting_event=meeting_event
        )


class CancelMeetingEvent(graphene.Mutation):
    ok = graphene.Boolean(required=True)

    @login_required
    def mutate(self, info):
        meeting = active_meetings(info.context.user.id).first()
        if not meeting:
            raise GQLError('No active meeting')
        meeting.users.remove(info.context.user.id)
        return CancelMeetingEvent(ok=True)


class CancelMeetingRequest(graphene.Mutation):
    ok = graphene.Boolean(required=True)

    @login_required
    def mutate(self, info):
        request = awaiting_queries(info.context.user.id).first()
        if not request or request.is_matched:
            raise GQLError('No active request')
        request.is_matched = True
        request.save()
        return CancelMeetingRequest(ok=True)


class MeetingMutations(graphene.ObjectType):
    create_meeting_request = CreateMeetingRequest.Field(required=True)
    cancel_meeting_event = CancelMeetingEvent.Field(required=True)
    cancel_meeting_request = CancelMeetingRequest.Field(required=True)
