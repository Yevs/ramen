import graphene

from graphene_django import DjangoObjectType

from ..models import MeetingRequest, MeetingEvent


class MeetingRequestNode(DjangoObjectType):
    class Meta:
        model = MeetingRequest
        only_fields = [
            'id',
            'date',
            'start',
            'end',
            'user'
        ]


class MeetingEventNode(DjangoObjectType):
    class Meta:
        model = MeetingEvent
        only_fields = [
            'id',
            'users',
            'date',
            'start',
            'end'
        ]
