import graphene


class CreateMeetingRequestInput(graphene.InputObjectType):
    start = graphene.Int(required=True)
    end = graphene.Int(required=True)
    date = graphene.String(required=True)
