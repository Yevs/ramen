import graphene

from gql_app.exceptions import GQLNotFound

from .nodes import MeetingRequestNode, MeetingEventNode
from ..models import MeetingRequest, MeetingEvent
from ..bl import active_meetings, awaiting_queries


class MeetingQuery(graphene.ObjectType):
    meeting_request = graphene.Field(
        MeetingRequestNode,
        required=True,
        id=graphene.Int(required=True)
    )
    active_meeting = graphene.Field(
        MeetingEventNode
    )
    active_request = graphene.Field(
        MeetingRequestNode
    )

    def resolve_meeting(root, info, *, id):
        try:
            request = MeetingRequest.objects.get(id=id)
            return request
        except MeetingRequest.ObjectDoesNotExist:
            raise GQLNotFound()

    def resolve_active_meeting(root, info):
        meeting = active_meetings(info.context.user.id).first()
        if not meeting:
            return None
        return meeting

    def resolve_active_request(root, info):
        request = awaiting_queries(info.context.user.id).first()
        if not request:
            return None
        return request