from passport.gql.queries import PassportQuery
from meetings.gql.queries import MeetingQuery
from chats.gql.queries import ChatQuery


class Query(
    PassportQuery,
    MeetingQuery,
    ChatQuery,
):
    pass
