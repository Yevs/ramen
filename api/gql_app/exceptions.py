from abc import ABC


class GQLBaseError(ABC, Exception):
    pass


class GQLError(GQLBaseError):
    def __init__(self, msg=''):
        self.msg = msg


class GQLForbidden(GQLError):
    def __init__(self, msg='Forbidden'):
        super().__init__(msg)


class GQLNotFound(GQLError):
    def __init__(self, msg='Not Found'):
        super().__init__(msg)
