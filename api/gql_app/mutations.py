from meetings.gql.mutations import MeetingMutations
from chats.gql.mutations import ChatMutations


class Mutation(
    MeetingMutations,
    ChatMutations,
):
    pass
