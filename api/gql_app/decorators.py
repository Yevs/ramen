from .exceptions import GQLForbidden


def login_required(fn):
    def _resolver(root, info, *args, **kwargs):
        if not info.context.user.is_authenticated:
            raise GQLForbidden('You must be logged in')
        return fn(root, info, *args, **kwargs)

    return _resolver
