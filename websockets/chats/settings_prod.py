import os
from pathlib import Path

from dotenv import load_dotenv


env_path = Path('.') / 'config' / '.env'

load_dotenv(dotenv_path=env_path)


WS_AUTH_SECRET = os.environ['WS_AUTH_SECRET']

