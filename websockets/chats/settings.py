import os

WS_AUTH_SECRET = 'CHANGE_ME'


if os.getenv('APP_MODE', 'dev') == 'prod':
    from .settings_prod import *
