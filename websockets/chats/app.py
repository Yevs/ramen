import asyncio
import json

import asyncpg
import aioredis
import jwt
from jwt.exceptions import DecodeError

from sanic import Sanic
from sanic.response import text

from .settings import WS_AUTH_SECRET


MESSAGES_TOPIC = 'messages'


app = Sanic()


@app.listener('before_server_start')
async def setup(app, loop):
    app.pool = await asyncpg.create_pool(
        database='postgres',
        user='postgres',
        host='db',
    )


async def ws_pong(ws):
    while True:
        await ws.recv()
        await ws.send('pong')


async def msg_proxy(ws, channel, event_ids):
    while await channel.wait_message():
        data = (await channel.get()).decode('utf-8')
        payload = json.loads(data)
        if payload.get('meeting_event_id') in event_ids:
            await ws.send(data)


@app.websocket('/ws')
async def index(request, ws):
    try:
        data = await ws.recv()
        payload = jwt.decode(data, WS_AUTH_SECRET, algorithms=['HS256'])
        user_id = int(payload.get('user_id'))
    except (DecodeError, json.decoder.JSONDecodeError, ValueError):
        await ws.send('Could not decode token')
        return

    redis = await aioredis.create_redis('redis://redis:6379')
    channel = (await redis.subscribe(MESSAGES_TOPIC))[0]

    async with request.app.pool.acquire() as conn:
        sql = '''
            SELECT meeting_event.id
            FROM (
            meeting_event
            LEFT JOIN meeting_event_users
                ON meeting_event.id = meeting_event_users.meetingevent_id
            )
            WHERE meeting_event_users.user_id = $1

        '''
        records = await conn.fetch(sql, user_id)
        event_ids = set(record['id'] for record in records)

    await asyncio.wait({ws_pong(ws), msg_proxy(ws, channel, event_ids)})