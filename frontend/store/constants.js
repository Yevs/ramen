export const API_DOMAIN = process.env.API_DOMAIN || 'http://localhost:8000'
export const API_URL = process.env.API_URL || 'http://localhost:8000/graphql/'
export const WS_URL = process.env.WS_URL || 'ws://localhost:7000/ws'
