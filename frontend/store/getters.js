export default {
  profile: state => state.profile,
  messages: state => state.messages,
  apiDomain: state => state.apiDomain,
  token: state => state.token,
  activeMeeting: state => state.activeMeeting,
  activeRequest: state => state.activeRequest
}
