export default {
  setProfile(state, {profile}){
    state.profile = profile;
  },
  setMessages(state, {messages}){
    state.messages = state.messages.concat(messages);
  },
  setAuthToken(state, {token}){
    state.token = token
  },
  setActiveMeeting(state, {activeMeeting}) {
    state.activeMeeting = activeMeeting
  },
  markWsStarted(state, wsStarted) {
    state.wsStarted = wsStarted
  },
  pushMessage(state, {message}){
    console.log(message)
    state.messages.push(message)
  },
  setActiveRequest(state, activeRequest) {
    state.activeRequest = activeRequest;
  }
}
