import 'whatwg-fetch'
import router from '../main.js'
import { API_URL, WS_URL } from './constants'

export default {
  getProfile({commit, state}) {
    fetch(API_URL, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query: `{  viewer {user {firstName avatar id} status wsAuthToken} }` }),
      credentials: 'include'
    }).then(res => res.json())
      .then((response) => {
        console.log(response)
        commit("setProfile", {profile: response.data.viewer.user});
        commit("setAuthToken", {token: response.data.viewer.wsAuthToken});
        router.push(response.data.viewer.status.toLowerCase())
        // router.push("welcome")
    }).catch(res => console.log(res));
  },

  createEvent({commit, state}, {day, time}){
    fetch(API_URL, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query: `mutation ($input: CreateMeetingRequestInput!){
                                      createMeetingRequest(input: $input) {
                                          meetingEvent {
                                            id
                                          }
                                        }
                                    }
      `, variables: {
        input: {
          start: time[0],
          end: time[1],
          date: day
        }
      }}),
      credentials: 'include'
    }).then(res => res.json())
      .then((response) => {
        if (response.data.meetingEvent) {
          router.push("chat")
        } else {
          router.push('looking')
        }
    }).catch(res => console.log(res));
  },

  fetchActiveRequest({commit, state}) {
    fetch(API_URL, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `{
          activeRequest {
            start
            end
            date
          }
        }`
      }),
      credentials: 'include'
    })
    .then(res => res.json())
    .then(({ data: { activeRequest } }) => {
      commit('setActiveRequest', activeRequest)
    })
  },

  getChat({commit, state}){
    fetch(API_URL, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `{
          activeMeeting {
            id
            users {
              id
            }
            start
            end
            date
          }
          messages {
            id
            content
            sender {
              avatar
              id
            }
            meetingEvent {
              id
            }
            createdAt
          }
        }`
      }),
      credentials: 'include'
    }).then(res => res.json())
      .then(({ data: { messages, activeMeeting } }) => {
        commit("setMessages", {messages});
        commit('setActiveMeeting', {activeMeeting})
    }).catch(res => console.log(res));
  },

  cancelRequest() {
    fetch(API_URL, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      credentials: 'include',
      body: JSON.stringify({
        query: `mutation {
          cancelMeetingRequest {
            ok
          }
        }`
      })
    })
    .then(res => res.json())
    .then((() => {
      router.push('slot')
    }))
  },

  sendMessage({commit, state}, {message}){
    console.log("send")
    fetch(API_URL, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query: `mutation {
                                      sendMessage(content: "${message}") {
                                        message {
                                          id
                                        }
                                       }
                                     }
      `}),
      credentials: 'include'
    }).then(res => res.json())
      .then((response) => {
        console.log(response)
        // commit("setProfile", {profile: response.data.viewer.user});
        // router.push(response.data.viewer.status.toLowerCase())
    }).catch(res => console.log(res));
  },

  startSocket({commit, state}){
    const { token } = state
    if (!token || state.wsStarted) {
      return
    }
    commit('markWsStarted', true)
    let ws
    let reconnectTimeout
    const initWs = () => {
      ws = new WebSocket(WS_URL)
      let ping = 0;
      let pong = 0;
      const interval = window.setInterval(() => {
        ws.send('ping')
        ping++
        setTimeout(() => {
          if (ping - pong > 1) {
            try {
              ws.close()
            } catch(e) {

            }
          }
        }, 1000)
      }, 1000)
      ws.onopen = () => {
        window.clearTimeout(reconnectTimeout)
        ws.send(token)
      }
      ws.onmessage = (event) => {
        if (event.data === 'pong') {
          pong++
          return
        }
        commit("pushMessage", {message: JSON.parse(event.data)})
      }
      ws.onclose = () => {
        console.log('CLOSED, RECONNECT')
        window.clearInterval(interval)
        window.clearTimeout(reconnectTimeout)
        reconnectTimeout = setTimeout(initWs, 1000)
      }
    }
    initWs()
  },

  pollViewerStatus({commit, state}, { stopPolling }) {
    fetch(API_URL, {
      method: 'POST',
      credentials: 'include',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
          {
            viewer {
              status
            }
          }
        `
      })
    })
    .then(res => res.json())
    .then(({ data: { viewer: { status } } }) => {
      if (status !== 'LOOKING') {
        stopPolling()
        router.push(status.toLowerCase())
      }
    })
  },

  cancelEvent({commit, state}){
    console.log("send")
    fetch(API_URL, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query: `mutation {
                                       cancelMeetingEvent {
                                         ok
                                        }
                                      }
      `}),
      credentials: 'include'
    }).then(res => res.json())
      .then((response) => {
        console.log(response)
        // commit("setProfile", {profile: response.data.viewer.user});
        router.push("slot")
    }).catch(res => console.log(res));
  }

}
