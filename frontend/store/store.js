import Vue from 'vue';
import Vuex from 'vuex';

import actions from './actions';
import mutations from './mutations';
import getters from './getters';
import { API_DOMAIN } from './constants'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
      profile: "",
      messages: [{sender: {id: "gezer"}, id: "2431", content: "Мы подобрали тебе компанию. Вместо имён все видят псевдонимы участников. "},
                 {sender: {id: "gezer"}, id: "2431", content: "Напиши первое сообщение, чтобы договориться о встрече. (нотификаций пока нету, но скоро будут)"},
                ],
      apiDomain: API_DOMAIN,
      token: "",
      activeMeeting: null,
      wsStarted: false,
      activeRequest: null,
    },
    getters,
    mutations,
    actions
});
