import './css/main.scss';

const Vue = require('vue')
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import { store } from './store/store';

const Navigation = require('./components/Navigation.vue')
const Welcome = require('./components/Welcome.vue')
const Slot = require('./components/Slot.vue')
const Looking = require('./components/Looking.vue')
const Chat = require('./components/Chat.vue')
const VueScrollTo = require('vue-scrollto');

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(VueScrollTo)

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/welcome', component: Welcome },
    { path: '/slot', component: Slot },
    { path: '/looking', component: Looking },
    { path: '/chat', component: Chat },
    // { path: '/settings', component: Settings, meta: {auth: true}}
  ]
})

router.beforeEach((to, from, next) => {
  const authRequired = to.matched.some((route) => route.meta.auth)
  const activeRequired = to.matched.some((route) => route.meta.active)
  const authed = store.state.token
  const active = store.state.status == "approved"
  if (authRequired && !authed) {
    next('/login')
  } else if (activeRequired && !active) {
    // next('/settings')
  } else {
    next()
  }
})

export default router

new Vue({
  el: '#app',
  store,
  components: {
    Navigation: Navigation
  },
  router,
  data: function(){
    return {
      transitionName: ""
    }
  },
  watch: {
    '$route' (to, from) {
      const toDepth = to.path.split('/').length
      const fromDepth = from.path.split('/').length
      this.transitionName = toDepth < fromDepth ? 'slide-right' : 'slide-left'
    }
  }
})
