function hashFnv32a(str, seed) {
  var i, l,
      hval = (seed === undefined) ? 0x811c9dc5 : seed;

  for (i = 0, l = str.length; i < l; i++) {
      hval ^= str.charCodeAt(i);
      hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
  }
  
  return hval >>> 0;
}

const adjectives = [
  'Общительный',
  'Анонимный',
  'Похмельный',
  'Стеснительный',
  'Душевный',
  'Мажорный',
  'Подвыпивший', 
  'Поддатый',
  'Дерзкий', 
  'Игривый',
  'Озорной',
  'Забавный',
  'Напористый',
  'Тепленький',
  'Выпивший', 
  'Депрессивный',
  'Готовый',
  'Мятный',
]

const families = [{
  name: 'Логово кошачьих',
  subjects: [
    'Тигр',
    'Лев',
    'Котик',
    'Ягуар',
    'Гепард',
  ]
}, {
  name: 'Слет пернатых',
  subjects: [
    'Воробей',
    'Попугай',
    'Ворон',
    'Снегирь',
    'Стриж',
  ]
}, {
  name: 'Морское днище',
  subjects: [
    'Скат',
    'Коралл',
    'Планктон',
    'Кит',
    'Краб',
  ]
}, {
  name: 'Тусовка в джунглях',
  subjects: [
    'Шимпанзе',
    'Какаду',
    'Лемур',
    'Змей',
    'Скорпион',
  ]
}, {
  name: 'Сафари экспедиция',
  subjects: [
    'Верблюд',
    'Змей',
    'Жираф',
    'Бегемот',
    'Тигр',
  ]
}, {
  name: 'Арктическая вечеринка',
  subjects: [
    'Медмедь',
    'Тюлень',
    'Пингвин',
    'Песец',
    'Морж',
  ]
}]

export const getFamily = (meetingEventId) => {
  const meetingHash = hashFnv32a(`${meetingEventId}`)
  return families[meetingHash % families.length]
}

export const getName = (meetingEventId, userId) => {
  const hash = hashFnv32a(`${meetingEventId}_${userId}`)
  const family = getFamily(meetingEventId)
  const subject = family.subjects[hash % family.subjects.length]
  const adj = adjectives[hash % adjectives.length]
  return `${adj} ${subject}`
}