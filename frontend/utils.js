export const getDateDelta = (activeMeeting) => {
  const plannedDate = new Date(activeMeeting.date)
  const now = new Date()
  return now.getDate() - plannedDate.getDate()
}