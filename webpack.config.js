var path = require('path')
var webpack = require('webpack')
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require("html-webpack-plugin")
var HtmlWebpackPrefixPlugin = require("html-webpack-prefix-plugin")

var GS_BUCKET_URL = process.env.GS_BUCKET_URL || 'https://storage.googleapis.com/ramen-static/'

var plugins = [
  new webpack.LoaderOptionsPlugin({
    vue: {
      loaders: {
        sass: 'vue-style-loader!css-loader!sass?indentedSyntax'
      }
    }
  }),
  new ExtractTextPlugin(
    process.env.NODE_ENV === 'production' ?
    'main.[contentHash].css' :
    'main.css'
  ),
]

if (process.env.NODE_ENV === 'production') {
  plugins = [
    ...plugins,
    new HtmlWebpackPlugin({
      template: './index.html',
      filename: 'index.html',
      prefix: GS_BUCKET_URL
    }),
    new HtmlWebpackPrefixPlugin()
  ]
}

module.exports = {
  entry: './frontend/main.js',
  output: {
    path: path.join(__dirname, './build'),
    filename: process.env.NODE_ENV === 'production' ? 'bundle.[hash].js' : 'bundle.js'
  },
  devServer: {
    historyApiFallback: true,
    noInfo: false
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader'
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ['css-loader', 'sass-loader']
        })
      }
    ]
  },
  plugins,
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js'
    }
  }
}

if (process.env.NODE_ENV === 'production') {
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.EnvironmentPlugin({
      NODE_ENV: process.env.NODE_ENV,
      API_URL: 'https://gezer.co/graphql/',
      WS_URL: 'wss://gezer.co/ws',
      API_DOMAIN: 'https://gezer.co'
    }),
    // new webpack.optimize.UglifyJsPlugin({
    //   compress: {
    //     warnings: false
    //   }
    // }),
    // new webpack.optimize.OccurrenceOrderPlugin()
  ])
}
