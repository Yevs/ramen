.PHONY: help

help: ## Help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

deploy_static:
		yarn install --frozen-lockfile
		NODE_ENV=production yarn run build
		gsutil rsync -d build gs://ramen-static

deploy_prod:
		docker-compose -f docker-compose.prod.yml up -d --force-recreate --build
